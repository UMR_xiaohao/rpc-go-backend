package protocol

import "sync"

const (
	PACKAGE_ERROR = iota
	PACKAGE_FULL
	PACKAGE_LESS
)

var (
	curprotocol Protocol
	once        sync.Once
)

type Protocol interface {
	ReadHeader(pkg []byte, header *RpcMsgHeader) bool
	ParseReqMsg(pkg []byte, header *RpcCallHeader) bool
	ParseProxyReqMsg([]byte, *RpcProxyCallHeader) bool
	ParseRespMsg(pkg []byte, header *RpcCallRetHeader) bool
	ParseProxyRespMsg([]byte, *RpcProxyCallRetHeader) bool
	PackRespMsg(resp *ResponsePackage) ([]byte, int)
	PackReqMsg(req *RequestPackage) ([]byte, int)
	PackProxyReqMsg(req *ProxyRequestPackage) ([]byte, int)
	PackProxyRespMsg(resp *ProxyRespPackage) ([]byte, int)
}

//初始化，对接多种协议格式可以
func init() {
	once.Do(func() {
		curprotocol = &binaryProtocol{}
	})
}

func SetProtocl(cus_proto Protocol) {
	curprotocol = cus_proto
}

func ReadHeader(pkg []byte) *RpcMsgHeader {
	if curprotocol == nil {
		return nil
	}

	header := &RpcMsgHeader{}
	if curprotocol.ReadHeader(pkg, header) == false {
		return nil
	}

	return header
}

func ReadCallHeader(pkg []byte) *RpcCallHeader {
	if curprotocol == nil {
		return nil
	}

	header := &RpcCallHeader{}
	if curprotocol.ParseReqMsg(pkg, header) == false {
		return nil
	}
	return header
}

func ReadProxyCallHeader(pkg []byte) *RpcProxyCallHeader {
	if curprotocol == nil {
		return nil
	}
	header := &RpcProxyCallHeader{}
	//sif cur protocol.Parse
	if curprotocol.ParseProxyReqMsg(pkg, header) == false {
		return nil
	}
	return header
}

func ReadRetHeader(pkg []byte) *RpcCallRetHeader {
	if curprotocol == nil {
		return nil
	}

	header := &RpcCallRetHeader{}
	if curprotocol.ParseRespMsg(pkg, header) == false {
		return nil
	}
	return header
}

func ReadProxyRetHeader(pkg []byte) *RpcProxyCallRetHeader {
	if curprotocol == nil {
		return nil
	}

	header := &RpcProxyCallRetHeader{}
	if curprotocol.ParseProxyRespMsg(pkg, header) == false {
		return nil
	}
	return header
}

func PackRespMsg(resp *ResponsePackage) ([]byte, int) {
	return curprotocol.PackRespMsg(resp)
}

func PackProxyRespMsg(resp *ProxyRespPackage) ([]byte, int) {
	return curprotocol.PackProxyRespMsg(resp)
}

func PackReqMsg(req *RequestPackage) ([]byte, int) {
	return curprotocol.PackReqMsg(req)
}

func PackProxyReqMsg(req *ProxyRequestPackage) ([]byte, int) {
	return curprotocol.PackProxyReqMsg(req)
}
