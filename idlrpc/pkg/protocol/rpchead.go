package protocol

import (
	"encoding/binary"

	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/internal/common"
)

const (
	INVALID_MSG      uint32 = iota //0 无效类型
	RequestMsg                     // 1 调用请求
	ResponseMsg                    // 2 调用返回
	ProxyRequestMsg                // 3 proxy模式调用请求
	ProxyResponseMsg               // 4 proxy 模式请求回调
	NotRpcMsg                      // 5 非RPC协议
)

const (
	IDL_SUCCESS uint32 = iota + 1
	IDL_SERVICE_NOT_FOUND
	IDL_SERVICE_ERROR
	IDL_RPC_TIME_OUT
)

var (
	RpcHeadSize       int
	CallHeadSize      int
	RespHeadSize      int
	ProxyCallHeadSize int
	ProxyRetHeadSize  int
)

// RpcMsgHeader 协议包头 协议类型 协议长度
type (
	GlobalIndexType uint32 //global index type
	RpcMsgHeader    struct {
		Length uint32 //整个包长，包含头
		Type   uint32 //调用 返回 非rpc 请求
	}
	RpcCallHeader struct {
		RpcMsgHeader
		ServiceUUID uint64 //服务UUID
		ServerID    uint32 //服务器实例ID
		CallID      uint32 //代理调用id
		MethodID    uint32 //方法id
	}

	RpcProxyCallHeader struct {
		RpcMsgHeader
		ServiceUUID uint64          //服务UUID
		ServerID    uint32          //服务器实例ID
		CallID      uint32          //代理调用id
		MethodID    uint32          //方法id
		GlobalIndex GlobalIndexType //代理节点标识
		OneWay      uint16          // 是否是one way节点
	}
	RpcCallRetHeader struct {
		RpcMsgHeader
		ServerID  uint32
		CallID    uint32
		ErrorCode uint32
	}

	RpcProxyCallRetHeader struct {
		RpcMsgHeader
		ServerID    uint32          //服务实例id
		CallID      uint32          //调用id对端赋值
		ErrorCode   uint32          //错误代码
		GlobalIndex GlobalIndexType //代理节点标识
	}
)

func init() {
	RpcHeadSize = binary.Size(RpcMsgHeader{})
	CallHeadSize = binary.Size(RpcCallHeader{})
	RespHeadSize = binary.Size(RpcCallRetHeader{})
	ProxyCallHeadSize = binary.Size(RpcProxyCallHeader{})
	ProxyRetHeadSize = binary.Size(RpcProxyCallRetHeader{})
}

func BuildRespHeader(resp *ResponsePackage, srvID uint32, callID uint32, errcode uint32) {
	if resp == nil {
		return
	}

	if resp.Header == nil {
		resp.Header = &RpcCallRetHeader{}
	}

	resp.Header.Type = ResponseMsg
	resp.Header.Length = uint32(RespHeadSize) + uint32(len(resp.Buffer))
	resp.Header.ServerID = srvID
	resp.Header.CallID = callID
	resp.Header.ErrorCode = errcode
}

func BuildProxyCallHeader(header *RpcCallHeader, globalIndex GlobalIndexType) *RpcProxyCallHeader {
	ph := &RpcProxyCallHeader{
		RpcMsgHeader{
			Length: header.Length - uint32(CallHeadSize) + uint32(ProxyCallHeadSize),
			Type:   ProxyRequestMsg,
		},
		header.ServiceUUID,
		header.ServerID,
		header.CallID,
		header.MethodID,
		globalIndex,
		0,
	}
	return ph
}

func BuildProxyRespHeader(resp *ProxyRespPackage, srvID uint32, callID uint32, errcode uint32, globalIndex GlobalIndexType) {
	if resp == nil {
		return
	}

	if resp.Header == nil {
		resp.Header = &RpcProxyCallRetHeader{}
	}

	resp.Header.Type = ProxyResponseMsg
	resp.Header.Length = uint32(ProxyRetHeadSize) + uint32(len(resp.Buffer))
	resp.Header.ServerID = srvID
	resp.Header.CallID = callID
	resp.Header.ErrorCode = errcode
	resp.Header.GlobalIndex = globalIndex
}

func BuildNotFound(req *RpcCallHeader) (resp *ResponsePackage) {
	if req == nil {
		return
	}
	resp = &ResponsePackage{
		Header: &RpcCallRetHeader{},
	}
	resp.Header.Type = ResponseMsg
	resp.Header.Length = uint32(RespHeadSize)
	resp.Header.ErrorCode = IDL_SERVICE_NOT_FOUND
	resp.Header.CallID = req.CallID
	resp.Header.ServerID = common.InvalidStubId
	return
}

func BuildProxyNotFound(req *RpcProxyCallHeader) (resp *ProxyRespPackage) {
	if req == nil {
		return
	}
	resp = &ProxyRespPackage{
		Header: &RpcProxyCallRetHeader{},
	}
	resp.Header.Type = ProxyResponseMsg
	resp.Header.Length = uint32(ProxyRetHeadSize)
	resp.Header.ErrorCode = IDL_SERVICE_NOT_FOUND
	resp.Header.CallID = req.CallID
	resp.Header.ServerID = common.InvalidStubId
	resp.Header.GlobalIndex = req.GlobalIndex
	return
}

//BuildException build run exception response
func BuildException(callID uint32) (resp *ResponsePackage) {
	resp = &ResponsePackage{
		&RpcCallRetHeader{
			RpcMsgHeader{
				uint32(RespHeadSize),
				ResponseMsg,
			},
			0,
			callID,
			IDL_SERVICE_ERROR,
		},
		nil,
	}
	return resp
}

// BuildTimeOut TODO use static variables
func BuildTimeOut(callID uint32) (resp *ResponsePackage) {
	resp = &ResponsePackage{
		&RpcCallRetHeader{
			RpcMsgHeader{
				uint32(RespHeadSize),
				ResponseMsg,
			},
			0,
			callID,
			IDL_RPC_TIME_OUT,
		},
		nil,
	}

	return
}
