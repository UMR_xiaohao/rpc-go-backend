package idlrpc

import (
	"context"

	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/log"
)

type (
	Options struct {
		ctx        context.Context
		logger     log.ILogger
		stackTrace bool
	}
	Option func(*Options)
)

func WithUserData(key, val interface{}) Option {
	return func(o *Options) {
		o.ctx = context.WithValue(o.ctx, key, val)
	}
}

func WithLogger(logger log.ILogger) Option {
	return func(o *Options) {
		o.logger = logger
	}
}

func WithStackTrace(open bool) Option {
	return func(o *Options) {
		o.stackTrace = open
	}
}
